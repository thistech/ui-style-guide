import Ember from 'ember';

export default Ember.Controller.extend({

    actions: {

        hideHelpPopover: function () {
            'use strict';
            this.get('controllers.helpPopover').hide();
        }
    },

    applicationName: 'UI Style Guide',

    helpPopover: Ember.inject.controller(),

    helpPopoverContent: Ember.computed.alias('helpPopover.helpContent'),

    isHelpPopupVisible: Ember.computed.alias('helpPopover.isVisible'),

    /**
     * Displays a modal pop-up window that displays online help content.
     *
     * @param {string} helpContent          HTML-formatted help content.
     * @param {number} left                 Popup window's left offset.
     * @param {number} top                  Popup window's top offset.
     */
    showHelpPopover: function (helpContent, left, top) {
        'use strict';
        var helpPopover = this.get('helpPopover');
        helpPopover.set('anchorLocation', { left: left - 25, top: top + 17 });
        helpPopover.set('helpContent', helpContent);
        helpPopover.set('isVisible', true);
    }
});
