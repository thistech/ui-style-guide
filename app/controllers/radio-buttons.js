import Ember from 'ember';

export default Ember.Controller.extend({

    actions: {

        changeFirstValue: function () {
            'use strict';
            this.set('firstValue', !this.get('firstValue'));
        },

        changeSecondValue: function () {
            'use strict';
            this.set('secondValue', !this.get('secondValue'));
        },

        changeThirdValue: function () {
            'use strict';
            this.et('thirdValue', !this.get('thirdValue'));
        }
    },

    firstValue: true,

    secondValue: false,

    thirdValue: false,

    onFirstValueChanged: Ember.observer('firstValue', function () {
        'use strict';
        this.updateValue('firstValue', ['secondValue', 'thirdValue']);
    }),

    onSecondValueChanged: Ember.observer('secondValue', function () {
        'use strict';
        this.updateValue('secondValue', ['firstValue', 'thirdValue']);
    }),

    onThirdValueChanged: Ember.observer('thirdValue', function () {
        'use strict';
        this.updateValue('thirdValue', ['firstValue', 'secondValue']);
    }),

    updateValue: function (valueChanged, valuesToUnset) {
        'use strict';
        var controller = this;
        if (this.get(valueChanged)) {
            valuesToUnset.forEach(function (valueName) {
                controller.set(valueName, false);
            });
        }
    }
});

