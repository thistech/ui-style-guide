import Ember from 'ember';

export default Ember.Controller.extend({

    actions: {

        showHelpPopover: function (element, content) {
            'use strict';
            var applicationController = this.get('application'),
                offset = element.offset(),
                top = element.offsetParent().offset().top;
            applicationController.showHelpPopover(content, offset.top - top + element.height() + 5, offset.left + 10);
            Ember.$('#cc-help-popover').show();
            Ember.$('#cc-help-popover').toggleClass('cc-modal-dialog');
        }
    },

    application: Ember.inject.controller(),

    html: '<b>URI (Universal Resource Identifier)</b><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
});
