import Ember from 'ember';

export default Ember.Controller.extend({

    anchorLocation: null,

    application: Ember.inject.controller(),

    html: '',

    isVisible: false,

    hide: function () {
        'use strict';
        this.set('isVisible', false);
        this.get('application').set('isPopoverVisible', false);
    }
});
