import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
    location: config.locationType
});

Router.map(function () {
    'use strict';
    this.resource('alerts');
    this.resource('buttons');
    this.resource('checkboxes');
    this.resource('create-or-edit-pages');
    this.resource('date-and-time');
    this.resource('date-picker');
    this.resource('detail-pages');
    this.resource('drop-down-menus');
    this.resource('errors');
    this.resource('expand-contract-controls');
    this.resource('footer');
    this.resource('form-elements');
    this.resource('form-layout');
    this.resource('header-and-navigation-bar');
    this.resource('headings-and-content');
    this.resource('help-flyouts');
    this.resource('historical-query');
    this.resource('introduction');
    this.resource('label-styling');
    this.resource('login-page');
    this.resource('main-or-hub-pages');
    this.resource('modal-dialogs');
    this.resource('modal-dialog-general');
    this.resource('modal-dialog-help');
    this.resource('modal-dialog-interstitial');
    this.resource('multiple-select');
    this.resource('no-content-states');
    this.resource('operations-log');
    this.resource('progressive-ui');
    this.resource('radio-buttons');
    this.resource('search');
    this.resource('table-general');
    this.resource('table-pagination');
    this.resource('tables');
    this.resource('templates');
    this.resource('text-fields');
    this.resource('time-zones');
    this.resource('tip-flyouts');
});

export default Router;
